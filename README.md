![get](https://upload.wikimedia.org/wikipedia/en/thumb/6/6b/Redis_Logo.svg/1200px-Redis_Logo.svg.png)

1. what is redis

Redis stand for remote directory server. fast,open source , in memory ,key value data store.The project started when Salvatore Sanfilippo, the original developer of Redis, wanted to improve the scalability of his Italian startup. From there, he developed Redis, which is now used as a database, cache, message broker, and queue.Redis delivers sub-millisecond response times, enabling millions of requests per second for real-time applications in industries like gaming, ad-tech, financial services, healthcare, and IoT. Today, Redis is one of the most popular open source engines today, named the "Most Loved" database by Stack Overflow for five consecutive years. Because of its fast performance, Redis is a popular choice for caching, session management, gaming, leaderboards, real-time analytics, geospatial, ride-hailing, chat/messaging, media streaming, and pub/sub apps.

2. Redis configration

## Redis configrations

Redis is able to start without a configuration file using built in default configuration. However this setup only recommended on for test purpose.

The proper way to configure redis provide redis.conf file.

## Redis Persistence

Redis persistence refers to the writing of data to durable storage,such as a solid-state disk (SSD). Redis provides a range of persistence options

- RDB (Redis database): RDB persistence performs point-in-time snapshots of your dataset at specified intervals.

- AOF (Append Only File): AOF persistence logs every write operation received by the server. These operations can then be replayed again at server startup, reconstructing the original dataset. Commands are logged using the same format as the Redis protocol itself.

- No persistence: You can disable persistence completely. This is sometimes used when caching.
 
- RDB + AOF: You can also combine both AOF and RDB in the same instance.
